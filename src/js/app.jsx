/* global React */

/**
 App entry point

 **/
import DataService from './core/data-service';
import Statement from './components/statement';


// Request data:
let service = new DataService('https://still-scrubland-9880.herokuapp.com');
service.fetchStatement();

// Render components
React.render(<Statement />, document.querySelector('#container'));
