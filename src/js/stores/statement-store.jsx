import {ServiceConstants, ViewStates} from '../core/app-constants';
import AppDispatcher from '../core/dispatcher';
import Package from './statement-constants'
import EventEmitter from 'events';


const CHANGE_EVENT = 'CHANGE';

let _data = {};
let _packages = undefined;
let _dispatchToken = undefined;

/** HELPERS **/
/**
 * Compose package objects containing semantically similar data
 * ToDo: Should this be a concern of the components?
 * @private
 */
function _createPackages() {
  _packages = {};
  _.forEach(Package, function (type) {
    _packages[type] = (_.filter(_data.package.subscriptions, function (item) {
      return item.type.toLowerCase() === type;
    }) || [])[0];
  });

  // Extras:
  var _package = _packages[Package.TALK];
  if (!!_package) {
    _package.charges = _data.callCharges;
  }
  _package = _packages[Package.WATCH];
  if (!!_package) {
    _package.skyStore = _data.skyStore;
  }
}

/**
 * Return cloned object to ensure immutability
 * @param obj
 * @private
 */
function _clone(obj) {
  if (!obj) return undefined;
  return JSON.parse(JSON.stringify(obj));
}

class StatementStore extends EventEmitter {

  constructor() {
    super();

    // Set up Dispatcher connections
    _dispatchToken = AppDispatcher.register(this.handleAction.bind(this));
  }

  connect(callback) {
    this.on(CHANGE_EVENT, callback);
  }

  disconnect(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }

  handleAction(action) {

    switch (action.type) {

      // Bill data load
      case ServiceConstants.LOAD_COMPLETE:
        // if bill data doesn't contain what we expect, don't notify..
        if(!this.isValidBill(action.payload)) {
          return;
        }

        // process and store data
        _data = action.payload;
        _data.statement.total = _data.total;
        _createPackages();
        break;

      default:
        //console.log(this + '::handleAction(), action.type + ' not handled');
        return;
    }

    // notify views
    this.emit(CHANGE_EVENT);
  }

  // Validate loaded data:
  isValidBill(_data) {
    return !!_data.package && !!_data.package.subscriptions;
  }

  // -------------- DATA ACCESSORS -------------- //
  getPackages() {
    return _clone(_packages);
  }

  getSummary() {
    return _clone(_data.statement);
  }

  toString() {
    return 'StatementStore';
  }

}

export default new StatementStore();
