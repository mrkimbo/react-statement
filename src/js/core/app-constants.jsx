const ServiceConstants = {
  LOAD_REQUEST: 'load-request',
  LOAD_COMPLETE: 'load-complete',
  LOAD_ERROR: 'load-error'
};

const ViewStates = {
  PENDING: 'pending',
  ENABLED: 'enabled',
  DISABLED: 'disabled'
};

const Date_Format = '';
const Currency = '£';
const Decimal_Precision = 2;


export {
  ServiceConstants,
  ViewStates,
  Date_Format,
  Currency,
  Decimal_Precision
};
