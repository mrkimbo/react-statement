/* global Flux */

class AppDispatcher extends Flux.Dispatcher {

  dispatch(action) {
    //console.log(this + '::dispatch()', action.type);
    super.dispatch(action);
  }

  toString() {
    return 'AppDispatcher';
  }

}

export default new AppDispatcher();
