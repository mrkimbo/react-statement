/* global fetch */

import AppDispatcher from '../core/dispatcher';
import {ServiceConstants} from '../core/app-constants';

let _endpoint = undefined;

class DataService {

  constructor(endpoint) {
    _endpoint = endpoint.replace(/\/?$/, '');

    this._handleSuccess = this._handleSuccess.bind(this);
    this._handleError = this._handleError.bind(this);
  }

  fetchStatement() {
    fetch(_endpoint + '/bill.json')
      .then(this._handleSuccess)
      .catch(this._handleError);
  }

  _handleSuccess(response) {
    if (!response.ok) {
      throw new Error(response.statusText);
      return;
    }

    response.json()
      .then(function (data) {
        AppDispatcher.dispatch({
          type: ServiceConstants.LOAD_COMPLETE,
          payload: data
        });
      });
  }

  _handleError(err) {
    AppDispatcher.dispatch({
      type: ServiceConstants.LOAD_ERROR,
      payload: err
    });
  }

  toString() {
    return 'DataService';
  }
}

export default DataService;
