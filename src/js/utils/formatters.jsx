import * as Config from '../core/app-constants';

const MONTH_NAMES = [
  'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
  'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
];
const DAY_NAMES = [
  'Mon', 'Tues', 'Weds', 'Thurs',
  'Fri', 'Sat', 'Sun'
];

function pad(n) {
  return n <= 9 ? ('0' + n) : n;
}

function formatDate(dateStr, pattern) {
  let d = new Date(dateStr);
  return (pattern || 'd/m/Y')
    .replace(/d/g, pad(d.getDate()))
    .replace(/D/g, DAY_NAMES[d.getDay()])
    .replace(/m/g, pad(d.getMonth()+1))
    .replace(/M/g, MONTH_NAMES[d.getMonth()])
    .replace(/Y/gi, d.getFullYear());
}

function formatCurrency(amount) {
  return Config.Currency + (amount || 0).toFixed(Config.Decimal_Precision);
}

export {
  formatDate,
  formatCurrency
};
