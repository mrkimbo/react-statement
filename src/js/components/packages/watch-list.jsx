/* global React */

import {formatCurrency} from '../../utils/formatters';

class CallList extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    console.log(this + '::render()', this.props);

    return (
      <div className="watch-list table">
        <div className="header">
          <span>Title</span>
          <span>Cost</span>
        </div>
        <ul>
          {
            _.map(this.props.items, function (item, idx) {
              return <li key={idx}>
                <div>
                  {item.title}
                  {formatCurrency(item.cost)}
                </div>
              </li>;
            })
          }
        </ul>
      </div>
    );
  }

  toString() {
    return 'WatchList';
  }

}

export default CallList;
