/* global React */

import {formatCurrency} from '../../utils/formatters';
import WatchList from './watch-list';

class WatchPackage extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    console.log(this + '::render()', this.props);

    var rentals = (this.props.skyStore.rentals || []).reduce(function(value, item) {
        return value + item.cost;
      }, 0);
    var extras = (this.props.skyStore.buyAndKeep || []).reduce(function(value, item) {
        return value + item.cost;
      }, 0);

    return (
      <section id="watch">
        <h3>Watch</h3>

        <div className="title">{this.props.name}</div>
        <div className="total">
          <span>Package cost: </span>{formatCurrency(this.props.cost)}
        </div>
        <div className="total">
          <span>Rentals: </span>{formatCurrency(rentals)}
          <WatchList items={this.props.skyStore.rentals} />
        </div>
        <div className="total">
          <span>Buy And Keep: </span>{formatCurrency(extras)}*
          <WatchList items={this.props.skyStore.buyAndKeep} />
        </div>
        <aside>
          <p className="small">* Items billed separately</p>
        </aside>
      </section>
    );
  }

  toString() {
    return 'WatchPackage';
  }
}

export default WatchPackage;
