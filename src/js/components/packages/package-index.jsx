import TalkPackage from './talk-package';
import WatchPackage from './watch-package';
import WebPackage from './web-package';

const AVAILABLE_PACKAGES = {
  TALK: TalkPackage,
  WATCH: WatchPackage,
  WEB: WebPackage
};

export default AVAILABLE_PACKAGES;
