/* glboal React */

import {formatCurrency} from '../../utils/formatters';

class CallList extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    console.log(this + '::render()', this.props);

    var classes = ['calls'];
    if(this.props.expanded) classes.push('expanded');

    return (
      <div className={classes.join(' ')}>
        <div className="header">
          <span>Number</span>
          <span>Duration</span>
          <span>Cost</span>
        </div>
        <ul>
          {
            _.map(this.props.calls, function (item, idx) {
              return <li key={idx}>
                <div>
                  {item.called}
                  {item.duration}
                  {formatCurrency(item.cost)}
                </div>
              </li>;
            })
          }
        </ul>
      </div>
    );
  }

  toString() {
    return 'CallList';
  }

}

export default CallList;
