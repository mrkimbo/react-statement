/* global React */

import {formatCurrency} from '../../utils/formatters';

class WebPackage extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    console.log(this + '::render()', this.props);

    var extras = 0;

    return (
      <section id="web">
        <h3>Web</h3>

        <div className="title">{this.props.name}</div>
        <div className="total">
          <span>Package cost: </span>{formatCurrency(this.props.cost)}
        </div>
        <div className="total">
          <span>Extra charges: </span>{formatCurrency(extras)}
        </div>
      </section>
    );
  }

  toString() {
    return 'WebPackage';
  }
}

export default WebPackage;
