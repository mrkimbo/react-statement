/* global React */

import {formatCurrency} from '../../utils/formatters';
import CallList from './call-list';


class TalkPackage extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      showCalls: false
    };

    this.toggleCalls = this.toggleCalls.bind(this);
  }

  toggleCalls(evt) {
    evt.preventDefault();
    this.setState({
      showCalls: !this.state.showCalls
    });
  }

  render() {
    console.log(this + '::render()', this.props);

    var extras = this.props.charges.total;
    var toggleLabel = (this.state.showCalls ? 'Hide' : 'Show');

    return (
      <section id="talk">
        <h3>Talk</h3>

        <div className="title">{this.props.name}</div>
        <div className="total">
          <span>Package cost: </span>{formatCurrency(this.props.cost)}
        </div>
        <div className="total">
          <span>Extra charges: </span>{formatCurrency(extras)}
        </div>
        <a href="#" className="toggle" onClick={this.toggleCalls}>{toggleLabel} calls</a>
        <CallList {...this.props.charges} expanded={this.state.showCalls}/>
      </section>
    );
  }

  toString() {
    return 'TalkPackage';
  }
}

export default TalkPackage;
