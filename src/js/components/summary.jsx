/* global React */

import {formatDate, formatCurrency} from '../utils/formatters';
import * as Config from '../core/app-constants';

class Summary extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    //console.log(this + '::render()', this.props);

    return (
      <section id="summary">
        <h3>Summary</h3>

        <div><span>Statement date: </span>{formatDate(this.props.generated, Config.Date_Format)}</div>
        <div>
          <span>Period covered: </span>
          <span>
            {formatDate(this.props.period.from, Config.Date_Format)}&nbsp;-&nbsp;
            {formatDate(this.props.period.to, Config.Date_Format)}
          </span>
        </div>
        <aside>
          <div><span>Total: </span>{formatCurrency(this.props.total)}</div>
          <div><span>Payment due: </span>{formatDate(this.props.due, Config.Date_Format)}</div>
        </aside>
      </section>
    );
  }

  toString() {
    return 'Summary';
  }
}

export default Summary;
