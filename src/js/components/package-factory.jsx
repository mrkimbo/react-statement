/* global React */

import Types from '../stores/statement-constants';
import Packages from './packages/package-index';

function create(type, props, key) {

  var Component;
  switch (type) {

    case Types.TALK:
      Component = Packages.TALK;
      break;

    case Types.WATCH:
      Component = Packages.WATCH;
      break;

    case Types.WEB:
      Component = Packages.WEB;
      break;

    default:
          return <div/>;
  }

  return <Component {...props} key={key}/>
}

export default {
  create
};
