/* global React */

import {ViewStates} from '../core/app-constants';
import {Packages} from '../stores/statement-constants';
import Store from '../stores/statement-store';

import Loader from './loader';
import Summary from './summary';
import PackageFactory from './package-factory';


class Statement extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: true
    };

    this._onStoreChange = this._onStoreChange.bind(this);
  }

  componentWillMount() {
    Store.connect(this._onStoreChange);
  }

  componentWillunMount() {
    Store.disconnect(this._onStoreChange);
  }

  _onStoreChange() {
    this.setState({
      loading: false,
      summary: Store.getSummary(),
      packages: Store.getPackages()
    });
  }

  render() {
    //console.log(this + '::render()', this.state);

    var fallback;
    if (this.state.loading) {
      fallback = <Loader/>;
    }

    var packages;
    if (!!this.state.packages) {
      packages = <div id="packages">
        { _.map(this.state.packages, function (item, idx) {
          return PackageFactory.create(item.type, item, idx);
        })}
      </div>;
    }

    return (
      <div id="statement">
        <div>
          <h1>Your Statement</h1>
          {fallback ||
          <Summary {...this.state.summary}/>
          }
          {packages}
        </div>
      </div>
    );
  }

  toString() {
    return 'Statement';
  }
}

export default Statement;
