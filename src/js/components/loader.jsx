/* global React */

class Loader extends React.Component {

  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div className="loader">
        <img src="img/ajax-loader.gif" />
      </div>
    );
  }

  toString() {
    return 'Loader';
  }

}

export default Loader;
