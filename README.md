# Statement Test

First outing using React and Flux. 
Consumes a json data source and displays the information within a series of view components.


## Installation
#### Install gulp and browserify globally:
```
$ npm install -g gulp browserify
```
#### Install npm and bower dependencies:
```
$ npm install
$ bower install
```

## Development
Compile and copy assets
```
$ gulp watch
```

## Building the project
Compile and copy assets
```
$ gulp build
```

## Viewing the project
Creates a basic server instance listening to http://localhost:8000
```
$ gulp serve
```