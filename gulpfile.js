var gulp = require('gulp');
var del = require('del');
var connect = require('gulp-connect');
var browserify = require('browserify');
var babelify = require('babelify');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var less = require('gulp-less');
var minifyCSS = require('gulp-minify-css');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var KarmaServer = require('karma').Server;


var config = {
  debug: true,
  src: 'src/',
  dest: 'deploy/',
  jsx: 'src/**/*.jsx',
  statc: 'src/**/*.{html,jpg,jpeg,png,gif,svg}',
  styles: 'src/**/main.less',
  components: [
    'src/components/es6-promise/promise.js',
    'src/components/fetch/fetch.js',
    'src/components/underscore/underscore.js',
    'src/components/react/react.js',
    'src/components/flux/dist/Flux.js'
  ]
};


function handleError(err) {
  console.log(err.toString());
  this.emit('end');
}

/**
 * Build JS / JSX
 */
gulp.task('compile', function () {
  return browserify({
    entries: config.src + 'js/app.jsx',
    extensions: ['.jsx'],
    debug: true
  })
    .transform(babelify)
    .bundle()
    .on('error', handleError)
    .pipe(source('app.min.js'))
    .pipe(buffer())
    .pipe(uglify({
      mangle: !config.debug,
      output: { beautify: config.debug }
    }))
    .pipe(gulp.dest(config.dest + '/js/'))
    .pipe(connect.reload());
});


/**
 * Create minified/concatenated shared-components bundle
 */
gulp.task('components', function () {
  gulp.src(config.components)
    .pipe(concat('components.min.js'))
    .pipe(uglify({
      mangle: !config.debug,
      output: { beautify: config.debug }
    }))
    .pipe(gulp.dest(config.dest + '/js/'))
    .pipe(connect.reload());
});

/**
 * clean last build
 */
gulp.task('clean', function () {
  return del(config.dest + '**/*');
});

/**
 * Compile less files
 */
gulp.task('less', function () {
  gulp.src(config.styles)
    .pipe(less())
    .pipe(minifyCSS())
    .on('error', handleError)
    .pipe(gulp.dest(config.dest))
    .pipe(connect.reload());
});

/**
 * Copy core files to deploy folder
 */
gulp.task('copy', ['clean'], function () {

  gulp.src(config.statc, {base: config.src})
    .pipe(gulp.dest(config.dest));
});

/**
 * Single run test (PhantomJS)
 */
gulp.task('test', function (done) {
  new KarmaServer({
    configFile: __dirname + '/karma.conf.js',
    browsers: ['PhantomJS'],
    singleRun: true
  }, done).start();
});

/**
 * Debug tests (Chrome)
 */
gulp.task('test-debug', function (done) {
  new KarmaServer({
    configFile: __dirname + '/karma.conf.js',
    browsers: ['Chrome'],
    singleRun: false
  }, done).start();
});

/**
 * Watch task - copies all src to deploy and then rebuilds on change
 */
gulp.task('watch', ['build'], function () {
  gulp.watch()
  gulp.watch(config.jsx, ['compile']);
  gulp.watch(config.styles, ['less']);
  gulp.watch(config.statc, ['copy']);
});

/**
 * Serve built project
 */
gulp.task('serve', function () {
  connect.server({
    port: 8000,
    root: config.dest,
    livereload: true
  });
});

gulp.task('build', ['copy', 'less', 'components', 'compile']);
gulp.task('default', ['watch']);
