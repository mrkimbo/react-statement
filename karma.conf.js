module.exports = function (config) {
  config.set({

    basePath: '',

    files: [
      'node_modules/karma-phantomjs-shim/shim.js',
      'src/components/es6-promise/promise.js',
      'src/components/fetch/fetch.js',
      'src/components/underscore/underscore.js',
      'src/components/react/react.js',
      'src/components/flux/dist/Flux.js',
      'src/**/*.jsx',
      'test/**/*.jsx'
    ],

    // exclude app entry point
    exclude: ['src/js/app.jsx'],

    preprocessors: {
      'src/**/*.jsx': ['browserify'],
      'test/**/*.jsx': ['browserify']
    },

    babelPreprocessor: {},

    browserify: {
      debug: true,
      extensions: ['.jsx'],
      transform: [
        'babelify'
      ],
    },
    frameworks: ['mocha', 'browserify'],
    reporters: ['mocha'],
    autoWatch: true

  });
};
