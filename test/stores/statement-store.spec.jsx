import {React, TestUtils, MockData, Sinon, expect} from '../setup';

// Modules to test:
import Store from '../../src/js/stores/statement-store';
import Packages from '../../src/js/stores/statement-constants';
import {ServiceConstants} from '../../src/js/core/app-constants';

describe('Stores:StatementStore', function () {

  it('should exist', function () {
    expect(Store).to.exist;
  });

  it('should expose summary data via a public API', function () {
    expect(Store.getSummary).to.be.defined;
  });

  it('should expose package data via a public API', function () {
    expect(Store.getPackages).to.be.defined;
  });

  describe('When data has been loaded', function () {

    before(function() {
      Store.emit = Sinon.spy(Store, 'emit');
    });

    after(function() {
      Store.emit.restore();
    })

    it('should dispatch a notification when data is loaded', function () {

      Store.handleAction({
        type: ServiceConstants.LOAD_COMPLETE,
        payload: MockData.RAW
      });

      expect(Store.emit.calledOnce).to.be.true;
      expect(Store.emit.calledWithExactly('CHANGE')).to.be.true;
    });

    it('should create semantically grouped data objects for packages', function () {
      var items = _.keys(Store.getPackages());
      expect(items).to.deep.equal(_.values(Packages));
    });

  });

});
