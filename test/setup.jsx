
import React from 'react/addons';
import TestUtils from 'react/lib/ReactTestUtils';
import {expect} from 'chai';
import Sinon from 'sinon';

const Find = {
  type: TestUtils.scryRenderedComponentsWithType,
  class: TestUtils.scryRenderedDOMComponentsWithClass,
  tag: TestUtils.scryRenderedDOMComponentsWithTag,
  number: function(el) {
    return parseFloat(el.textContent.replace(/[^\d\.]/g,''));
  }
};

const MockData = {

  RAW: {
    "statement": {
      "generated": "2015-01-11",
      "due": "2015-01-25",
      "period": {
        "from": "2015-01-26",
        "to": "2015-02-25"
      }
    },
    "total": 136.03,
    "package": {
      "subscriptions": [
        { "type": "tv", "name": "Variety with Movies HD", "cost": 50.00 },
        { "type": "talk", "name": "Sky Talk Anytime", "cost": 5.00 },
        { "type": "broadband", "name": "Fibre Unlimited", "cost": 16.40 }
      ]
    },
    "callCharges": {},
    "skyStore": {}
  },

  FORMATTED: {
    "loading": false,
    "summary": {
      "total": 100.00,
      "generated": "2015-01-11",
      "due": "2015-01-25",
      "period": {
        "from": "2015-01-26",
        "to": "2015-02-25"
      }
    },
    "packages": {
      "talk": {
        "type": "talk", "name": "Sky Talk Anytime", "cost": 1.00,
        "charges": {
          "total": 5.00,
          "calls": [
            { "called": "07716393769", "duration": "00:23:03", "cost": 4.00 }
          ]
        }
      },
      "watch": {
        "type": "tv", "name": "Variety with Movies HD", "cost": 3.00,
        "skyStore": {
          "total": 8.00,
          "rentals": [
            { "title": "50 Shades of Grey", "cost": 6.00 }
          ],
          "buyAndKeep": [
            { "title": "That's what she said", "cost": 7.00 }
          ]
        }
      },
      "web": {
        "type": "broadband", "name": "Fibre Unlimited", "cost": 9.00
      }
    }
  }
};

// Suppress console logging
window.log = console.log;
console.log = function(){};


export {
  React,
  Sinon,
  TestUtils,
  MockData,
  expect,
  Find
};
