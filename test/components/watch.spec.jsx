import {React, TestUtils, Find, MockData, expect} from '../setup';

// Modules to test:
import WatchPackage from '../../src/js/components/packages/watch-package';
import WatchList from '../../src/js/components/packages/watch-list';

describe('Components:WatchPackage', function () {

  var instance,
    data = MockData.FORMATTED;


  it('should create a valid component with the correct id', function () {
    instance = TestUtils.renderIntoDocument(<WatchPackage {...data.packages.watch}/>);
    expect(instance).to.exist;

    var root = Find.tag(instance, 'section')[0];
    expect(root).to.exist;
    expect(root.getDOMNode().id).to.equal('watch');
  });

  it('should contain a title', function () {
    var h3 = Find.tag(instance, 'h3')[0];
    expect(h3).to.exist;
    expect(h3.getDOMNode().textContent).to.equal('Watch');
  });

  it('should contain a package total', function () {
    var el = Find.class(instance, 'total').filter(function(node) {
      return (/package cost/i).test(node.getDOMNode().textContent);
    })[0];
    expect(el).to.exist;
    expect(Find.number(el.getDOMNode().lastElementChild)).to.equal(data.packages.watch.cost);
  });

  it('should contain a rentals list', function () {
    var el = Find.class(instance, 'total').filter(function(node) {
      return (/rentals/i).test(node.getDOMNode().textContent);
    })[0];
    expect(el).to.exist;

    var list = Find.type(el, WatchList)[0];
    expect(list).to.exist;
  });

  it('should contain a buy-and-keep list', function () {
    var el = Find.class(instance, 'total').filter(function(node) {
      return (/buy and keep/i).test(node.getDOMNode().textContent);
    })[0];
    expect(el).to.exist;

    var list = Find.type(el, WatchList)[0];
    expect(list).to.exist;
  });

});
