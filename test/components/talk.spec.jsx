import {React, TestUtils, Find, MockData, expect} from '../setup';

// Modules to test:
import TalkPackage from '../../src/js/components/packages/talk-package';
import CallList from '../../src/js/components/packages/call-list';

describe('Components:TalkPackage', function () {

  var instance,
    data = MockData.FORMATTED;


  it('should create a valid component with the correct id', function () {
    instance = TestUtils.renderIntoDocument(<TalkPackage {...data.packages.talk}/>);
    expect(instance).to.exist;

    var root = Find.tag(instance, 'section')[0];
    expect(root).to.exist;
    expect(root.getDOMNode().id).to.equal('talk');
  });

  it('should contain a title', function () {
    var h3 = Find.tag(instance, 'h3')[0];
    expect(h3).to.exist;
    expect(h3.getDOMNode().textContent).to.equal('Talk');
  });

  it('should contain a package total', function () {
    var el = Find.class(instance, 'total').filter(function(node) {
      return (/package cost/i).test(node.getDOMNode().textContent);
    })[0];
    expect(el).to.exist;
    expect(Find.number(el.getDOMNode().lastElementChild)).to.equal(data.packages.talk.cost);
  });

  it('should contain an extras total', function () {
    var el = Find.class(instance, 'total').filter(function(node) {
      return (/extra charges/i).test(node.getDOMNode().textContent);
    })[0];
    expect(el).to.exist;
    expect(Find.number(el.getDOMNode().lastElementChild)).to.equal(data.packages.talk.charges.total);
  });

  it('should contain a Call List component', function () {
    var el = Find.type(instance, CallList)[0];
    expect(el).to.exist;
  });

});
