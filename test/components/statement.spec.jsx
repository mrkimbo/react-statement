import {React, TestUtils, Find, expect, MockData} from '../setup';

// Modules to test:
import Loader from '../../src/js/components/loader';
import Statement from '../../src/js/components/statement';
import Summary from '../../src/js/components/summary';
import Packages from '../../src/js/components/packages/package-index';


describe('Components:Statement', function () {

  var instance,
    data = MockData.FORMATTED;

  it('should create a valid component with the correct id', function () {
    instance = TestUtils.renderIntoDocument(<Statement/>);
    expect(instance).to.exist;

    var root = Find.tag(instance, 'div')[0];
    expect(root).to.exist;
    expect(root.getDOMNode().id).to.equal('statement');
  });

  describe('Before data has been loaded', function () {

    it('should contain a title', function () {
      var h1 = Find.tag(instance, 'h1')[0];
      expect(h1).to.exist;
      expect(h1.getDOMNode().textContent).to.equal('Your Statement');
    });

    it('should contain a loader', function () {
      var loader = Find.type(instance, Loader)[0];
      expect(loader).to.exist;
    });
  });

  describe('Once data has loaded', function () {

    var child;

    before(function () {
      instance.setState(data);
    });

    it('should contain a Summary component', function () {
      child = Find.type(instance, Summary)[0];
      expect(child).to.exist;
    });

    it('should contain a Talk Package component', function () {
      child = Find.type(instance, Packages.TALK)[0];
      expect(child).to.exist;
    });

    it('should contain a Watch Package component', function () {
      child = Find.type(instance, Packages.WATCH)[0];
      expect(child).to.exist;
    });

    it('should contain a Web Package component', function () {
      child = Find.type(instance, Packages.WEB)[0];
      expect(child).to.exist;
    });
  });

});
