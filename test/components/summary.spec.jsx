import {React, TestUtils, Find, MockData, expect} from '../setup';

// Modules to test:
import Summary from '../../src/js/components/summary';


describe('Components:Summary', function () {

  var instance,
    data = MockData.FORMATTED;


  it('should create a valid component with the correct id', function () {
    instance = TestUtils.renderIntoDocument(<Summary {...data.summary}/>);
    expect(instance).to.exist;

    var root = Find.tag(instance, 'section')[0];
    expect(root).to.exist;
    expect(root.getDOMNode().id).to.equal('summary');
  });

  it('should contain a title', function () {
    var h3 = Find.tag(instance, 'h3')[0];
    expect(h3).to.exist;
    expect(h3.getDOMNode().textContent).to.equal('Summary');
  });

  it('should contain a statement date', function () {
    var el = Find.tag(instance, 'div').filter(function(node) {
      return (/statement date/i).test(node.getDOMNode().textContent);
    })[0];
    expect(el).to.exist;
  });

  it('should contain a bill range', function () {
    var el = Find.tag(instance, 'div').filter(function(node) {
      return (/period covered/i).test(node.getDOMNode().textContent);
    })[0];
    expect(el).to.exist;
  });

  it('should contain a due-by date', function () {
    var el = Find.tag(instance, 'div').filter(function(node) {
      return (/payment due/i).test(node.getDOMNode().textContent);
    })[0];
    expect(el).to.exist;
  });

  it('should contain a total', function () {
    var el = Find.tag(instance, 'div').filter(function(node) {
      return (/total/i).test(node.getDOMNode().textContent);
    })[0];
    expect(el).to.exist;
  });

});
