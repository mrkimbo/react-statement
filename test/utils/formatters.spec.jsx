import {React, TestUtils, expect} from '../setup';

// Modules to test:
import {formatDate, formatCurrency} from '../../src/js/utils/formatters';
import * as Config from '../../src/js/core/app-constants';


describe('Utils:Formatters', function() {

  it('should exist', function () {
    expect(formatDate).to.exist;
    expect(formatCurrency).to.exist;
  });

  describe('formatDate', function() {

    it('should format a date correctly', function() {
      var date = '2015-01-25';
      expect(formatDate(date)).to.equal('25/01/2015');
      expect(formatDate(date, 'd M Y')).to.equal('25 Jan 2015');
    });

  });

  describe('formatCurrency', function() {

    it('should format a number correctly', function() {

      // basic formatting tests:
      expect(formatCurrency(100.0123234)).to.equal('£100.01');
      expect(formatCurrency(1.5)).to.equal('£1.50');
    });

  });


});
