import {React, TestUtils, expect} from '../setup';

// Modules to test:
import DataService from '../../src/js/core/data-service';

describe('Core:DataService', function() {

  var service;

  before(function() {
    service = new DataService('https://still-scrubland-9880.herokuapp.com');
  });


  it('should exist', function () {
    expect(service).to.exist;
  });

  it('should expose a fetch method', function() {
    expect(service.fetchStatement).to.be.defined;
  });

  it('should dispatch an event on a successful data load', function() {


  });

  it('should dispatch an event on a failed data load', function() {


  });


});
